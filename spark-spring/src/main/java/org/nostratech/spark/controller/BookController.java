package org.nostratech.spark.controller;

import com.google.gson.Gson;
import org.nostratech.spark.payload.Book;
import org.nostratech.spark.services.BookService;

import static spark.Spark.*;

/**
 * agus w on 12/14/15.
 */
public class BookController {

    Gson gson = new Gson();

    public BookController(final BookService bookService) {

        post("/book", (request, response) -> {

            // process request
            final Book book = gson.fromJson(request.body(), Book.class);

            RequestHandler requestHandler = new RequestHandler() {
                @Override
                public Object processRequest() {

                    return bookService.post(book);
                }
            };

            response.status(200);
            response.type("application/json");
            return requestHandler.getResult("OK");
        });

        put("/book", (request, response) -> {

            // process request
            Book book = gson.fromJson(request.body(), Book.class);

            RequestHandler requestHandler = new RequestHandler() {
                @Override
                public Object processRequest() {
                    return bookService.put(book);
                }
            };

            response.status(200);
            response.type("application/json");
            return requestHandler.getResult("OK");
        });

        get("/book", (request, response) -> {
            RequestHandler requestHandler = new RequestHandler() {
                @Override
                public Object processRequest() {
                    return bookService.get();
                }
            };

            response.status(200);
            response.type("application/json");
            return requestHandler.getResult("OK");
        });

        get("/book/:id", (request, response) -> {
            RequestHandler requestHandler = new RequestHandler() {
                @Override
                public Object processRequest() {
                    return bookService.get(request.params(":id"));
                }
            };

            response.status(200);
            response.type("application/json");
            return requestHandler.getResult("OK");
        });

        delete("/book/:id", (request, response) -> {
            RequestHandler requestHandler = new RequestHandler() {
                @Override
                public Object processRequest() {
                    return bookService.delete(request.params(":id"));
                }
            };

            response.status(200);
            response.type("application/json");
            return requestHandler.getResult("OK");
        });
        // more routes
    }
}
