package org.nostratech.spark.services;

import org.nostratech.spark.payload.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import javax.sql.DataSource;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * agus w on 12/14/15.
 */
@Component("bookService")
public class BookService implements CRUDService<Book> {

    @Autowired
    DataSource dataSource;

    @Override
    public Boolean post(Book book) {

        int result;

        if (book.getId() == null) book.setId(UUID.randomUUID().toString());

        Sql2o sql2o = new Sql2o(dataSource);

        String sql = "INSERT INTO BOOK (SID, TITLE, AUTHOR) VALUES (:sid, :title, :author);";

        try (Connection con = sql2o.open()) {
            result = con.createQuery(sql)
                    .addParameter("sid", book.getId())
                    .addParameter("title", book.getTitle())
                    .addParameter("author", book.getAuthor())
                    .executeUpdate().getResult();
        }

        return (result > 0);
    }

    @Override
    public Boolean put(Book book) {
        int result;

        Sql2o sql2o = new Sql2o(dataSource);

        String sql = "UPDATE BOOK SET SID = :sid, TITLE = :title, AUTHOR = :author;";

        try (Connection con = sql2o.open()) {
            result = con.createQuery(sql)
                    .addParameter("sid", book.getId())
                    .addParameter("title", book.getTitle())
                    .addParameter("author", book.getAuthor())
                    .executeUpdate().getResult();
        }

        return (result > 0);
    }

    @Override
    public Boolean delete(String id) {
        int result;

        Sql2o sql2o = new Sql2o(dataSource);

        String sql = "DELETE FROM BOOK WHERE SID = :sid";

        try (Connection con = sql2o.open()) {
            result = con.createQuery(sql)
                    .addParameter("sid", id)
                    .executeUpdate().getResult();
        }

        return (result > 0);
    }

    @Override
    public Collection<Book> get() {

        List<Book> bookList = null;

        Sql2o sql2o = new Sql2o(dataSource);

        String sql =
                "SELECT sid as id, title as title, author as author FROM BOOK;";

        try (Connection con = sql2o.open()) {
            bookList = con.createQuery(sql).executeAndFetch(Book.class);
        }

        return bookList;
    }

    @Override
    public Book get(String id) {
        Book book = null;

        Sql2o sql2o = new Sql2o(dataSource);

        String sql =
                "SELECT sid as id, title as title, author as author FROM BOOK WHERE sid = :sid;";

        try (Connection con = sql2o.open()) {
            book = con.createQuery(sql).addParameter("sid", id).executeAndFetchFirst(Book.class);
        }

        return book;
    }

}
