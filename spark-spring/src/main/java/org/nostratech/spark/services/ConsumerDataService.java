package org.nostratech.spark.services;

import org.nostratech.spark.payload.Data;
import org.nostratech.spark.payload.DataMaster;
import org.nostratech.spark.payload.LongLat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by rennytanuwijaya on 2/5/16.
 */
@Component("consumerDataService")
public class ConsumerDataService implements CRUDService<LongLat> {

    @Autowired
    DataSource dataSource;


    @Override
    public Boolean post(LongLat data) {

        int result;

        Sql2o sql2o = new Sql2o(dataSource);

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String dateInString = dateFormat.format(date);

            String sql = "INSERT INTO LOCATION (IMEI, LONGITUDE, LATITUDE, SIGNAL) VALUES (:imei , :longitude , :latitude  , :signal);";

            try (Connection con = sql2o.open()) {
                result = con.createQuery(sql)
                        .addParameter("imei", data.getImei())
                        .addParameter("longitude", data.getLatitude())
                        .addParameter("latitude", data.getLatitude())
                        .addParameter("signal", data.getSignal())
                        .executeUpdate().getResult();
            }

//        .addParameter("time", dateInString)
        return result > 0;
    }

    @Override
    public Boolean put(LongLat data) {
        return null;
    }

    @Override
    public Boolean delete(String id) {
        return null;
    }

    @Override
    public Collection<LongLat> get() {
        List<LongLat> dataList = null;
        Sql2o sql2o = new Sql2o(dataSource);

        String sql =
                "SELECT IMEI, LONGITUDE, LATITUDE, TIME, SIGNAL FROM LOCATION ;";

        try (Connection con = sql2o.open()) {
            dataList = con.createQuery(sql).executeAndFetch(LongLat.class);
        }
        return dataList;
    }

    public Collection<LongLat> getSignalTime(String startTime, String endTime) {
        List<LongLat> dataList = null;
        Sql2o sql2o = new Sql2o(dataSource);

        String oldStartTimeString = startTime;
        LocalDateTime dateTimeStart = LocalDateTime.parse(oldStartTimeString, DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        String oldEndTimeString = endTime;
        LocalDateTime dateTimeEnd = LocalDateTime.parse(oldEndTimeString, DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));

        String newStringStartDate = dateTimeStart.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String newStringEndTime = dateTimeEnd.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String sql = "SELECT IMEI,LONGITUDE, LATITUDE, TIME, SIGNAL FROM LOCATION l WHERE TIME >= :startTime AND TIME < :endTime AND TIME = (SELECT MAX(TIME) FROM LOCATION GROUP BY IMEI HAVING IMEI = l.IMEI);";

        try (Connection con = sql2o.open()) {
            dataList = con.createQuery(sql).addParameter("startTime", newStringStartDate).addParameter("endTime",newStringEndTime).executeAndFetch(LongLat.class);
        }
        return dataList;
    }

    public Collection<LongLat> getSignalByImei(String startTime, String endTime, String imei) {
        List<LongLat> dataList = null;
        Sql2o sql2o = new Sql2o(dataSource);

        String oldStartTimeString = startTime;
        LocalDateTime dateTimeStart = LocalDateTime.parse(oldStartTimeString, DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        String oldEndTimeString = endTime;
        LocalDateTime dateTimeEnd = LocalDateTime.parse(oldEndTimeString, DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));

        String newStringStartDate = dateTimeStart.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String newStringEndTime = dateTimeEnd.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String sql =
                "SELECT IMEI, LONGITUDE, LATITUDE, TIME, SIGNAL FROM LOCATION WHERE IMEI = :imei AND TIME >= :startTime AND TIME < :endTime;";

        try (Connection con = sql2o.open()) {
            dataList = con.createQuery(sql).addParameter("imei", imei).addParameter("startTime", newStringStartDate).addParameter("endTime",newStringEndTime).executeAndFetch(LongLat.class);
        }
        return dataList;
    }

    @Override
    public LongLat get(String id) {
        return null;
    }
}
