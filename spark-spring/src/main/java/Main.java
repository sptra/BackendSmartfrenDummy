import org.nostratech.spark.controller.BookController;
import org.nostratech.spark.controller.DataConsumerController;
import org.nostratech.spark.services.BookService;
import org.nostratech.spark.services.ConsumerDataService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("Beans.xml");

        BookService bookService = (BookService) applicationContext.getBean("bookService");
        ConsumerDataService consumerDataService = (ConsumerDataService) applicationContext.getBean("consumerDataService");

        new BookController(bookService);
        new DataConsumerController(consumerDataService);
    }
}
